package com.classpath.inventoryservice.service;

import com.classpath.inventoryservice.model.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OrderProcessingService {

    @StreamListener(Sink.INPUT)
    public void processOrder(Order order){
      log.info(" Processing the order :: {}", order);
      log.info("Customer:: {} purchased from {}  of {} Rs on {} date",order.getCustomerName(), order.getMerchantName(), order.getTotalPrice(), order.getOrderDate());
    }
}