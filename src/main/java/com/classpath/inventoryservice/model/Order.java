package com.classpath.inventoryservice.model;

import lombok.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@ToString
@EqualsAndHashCode(of = "orderId")
@NoArgsConstructor
public class Order {

    private long orderId;
    private double totalPrice;
    private String merchantName;
    private String customerName;
    private String notes;
    private LocalDate orderDate;
    private Set<OrderLineItem> lineItems = new HashSet<>();
}