package com.classpath.inventoryservice.conroller;

import com.classpath.inventoryservice.model.Item;
import com.classpath.inventoryservice.service.SimpleSourceBean;
import com.classpath.inventoryservice.util.UserContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/inventory")
@Slf4j
public class InventoryController {

    private static int counter = 10000;

    @Autowired
    private SimpleSourceBean sourceBean;

    @GetMapping
    public int fetchItemsCount(){
        return counter;
    }

    @PostMapping
    public ResponseEntity<Integer> updateQty(){
      log.info("Came inside the update Qty method :: ");
      log.info(" Correlation Id from Order service :: {} ", UserContextHolder.getContext().getCorrelationId());
        --counter;
        return ResponseEntity.ok(counter);
    }

    @PostMapping("/items")
    public Item postItem(@RequestBody Item item){
        this.sourceBean.publishItem(item);
        return item;
    }

}